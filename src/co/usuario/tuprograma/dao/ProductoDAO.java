/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.dao;

import co.bs.handlersql.dao.BSParentDAO;
import co.usuario.tuprograma.bean.Persona;
import co.usuario.tuprograma.bean.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BELSOFT
 */
public class ProductoDAO extends BSParentDAO {

    public List<Producto> listarPorUnidad(String nombreUnidad) {
        List<Producto> productos = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement("select producto.* from producto\n"
                    + "inner join producto_unidad\n"
                    + "on producto.prod_id=producto_unidad.prod_id\n"
                    + "inner join unidad \n"
                    + "on unidad.unidad_id=producto_unidad.unidad_id \n"
                    + "and unidad.unidad_nombre=?");
            ps.setString(1, nombreUnidad);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setId(rs.getInt("prod_id"));
                producto.setNombre(rs.getString("prod_nombre"));
                productos.add(producto);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            productos = null;
        } finally {
            cerrarRsPs(rs, ps);
        }
        return productos;
    }

    public List<Producto> listarPorUnidad_(String nombreUnidad) {
        List<Producto> productos = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            
            ps = connection.prepareStatement(getSQL("LISTAR_POR_UNIDAD"));
            ps.setString(1, nombreUnidad);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setId(rs.getInt("prod_id"));
                producto.setNombre(rs.getString("prod_nombre"));
                productos.add(producto);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            productos = null;
        } finally {
            cerrarRsPs(rs, ps);
        }
        return productos;
    }
}
