/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.dao;

import co.bs.handlersql.dao.BSParentDAO;
import co.usuario.tuprograma.bean.Persona;
import java.util.LinkedList;
import java.util.List;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author BELSOFT
 */
public class TuClaseDAO extends BSParentDAO {

    public List<Persona> listar() {
        List<Persona> personas = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(getSQL("OBTENER_PERSONAS"));
            rs = ps.executeQuery();
            while(rs.next()){
                Persona persona=new Persona();
                persona.setPegeNombre(rs.getString("pege_nombres"));
                persona.setPegeApellidos(rs.getString("pege_apellidos"));
                personas.add(persona);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            personas=null;
        } finally{
            cerrarRsPs(rs, ps);
        }
        return personas;
    }
}
