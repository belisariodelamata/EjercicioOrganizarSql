package co.bs.handlersql.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * @author BS
 */
public class BSHandlerSql {

    /**
     * Mapa de Sql
     */
    private transient HashMap<String, String> sqlMap;
    /**
     * Direcccion del Archivo XML
     */
    private transient String xml = "";

    private boolean debugPath = true;

    public BSHandlerSql(String rutaPaqueteXml) {
        if (debugPath) {
            System.out.println("Si no quieres que aparezca este Path, coloca BSHandlerSql.debugPath=false");
            System.out.println(rutaPaqueteXml);
        }
        setXml(getClass().getResource(rutaPaqueteXml).getPath());
        examinarXml();
    }

    public String getSQL(String nombreSQL) {
        if (sqlMap != null) {
            return sqlMap.get(nombreSQL);
        } else {
            return busquedaInmediata(nombreSQL);
        }
    }

    private void setXml(String xml) {
        this.xml = xml;
    }

    /**
     * Verifica el XML y guarda todas las consultas en un Mapa
     */
    public void examinarXml() {
        sqlMap = new HashMap<String, String>();
        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(xml);
            Element root = document.getRootElement();
            for (Element bloque : root.getChildren()) {
                String nombreConsulta = bloque.getAttributeValue("name");
                String sql = bloque.getChildText("sql");
                sqlMap.put(nombreConsulta.toUpperCase(), sql.trim());
            }
        } catch (JDOMException ex) {
            Logger.getLogger(BSHandlerSql.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BSHandlerSql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String busquedaInmediata(String nombreSQL) {
        String sql = null;
        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(xml);
            Element root = document.getRootElement();
            for (Element bloque : root.getChildren()) {
                String nombreConsulta = bloque.getAttributeValue("name");
                if (nombreConsulta.trim().toUpperCase().equals(nombreSQL.trim().toUpperCase())) {
                    sql = bloque.getChildText("sql").trim();
                    return sql;
                }
            }
        } catch (JDOMException ex) {
            Logger.getLogger(BSHandlerSql.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BSHandlerSql.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

}
