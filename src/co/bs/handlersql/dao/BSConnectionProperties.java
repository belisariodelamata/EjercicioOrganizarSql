/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.bs.handlersql.dao;

import java.util.Properties;
import java.util.ResourceBundle;

/**
 *
 * @author BELSOFT
 */
public class BSConnectionProperties {

    public static BSConnectionProperties properties;

    private String driver;
    private String hostname;
    private String port;
    private String database;
    private String url;
    private String user;
    private String password;
    private boolean toConnect;

    static {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("application");
        properties = new BSConnectionProperties();
        properties.driver = resourceBundle.getString("driver");
        properties.hostname = resourceBundle.getString("hostname");
        properties.port = resourceBundle.getString("port");
        properties.database = resourceBundle.getString("database");
        properties.user = resourceBundle.getString("user");
        properties.password = resourceBundle.getString("password");
        properties.url = resourceBundle.getString("url")
                .replace("##hostname##", properties.hostname)
                .replace("##port##", properties.port)
                .replace("##database##", properties.database);
        properties.setToConnect(resourceBundle.getString("connect").equals("true"));
    }

    public String getDriver() {
        return driver;
    }

    public String getHostname() {
        return hostname;
    }

    public String getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    
    public static void main(String[] args) {

    }

    public boolean isToConnect() {
        return toConnect;
    }

    public void setToConnect(boolean toConnect) {
        this.toConnect = toConnect;
    }
}
