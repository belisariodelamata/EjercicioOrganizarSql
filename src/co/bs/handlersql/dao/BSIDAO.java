package co.bs.handlersql.dao;

import java.util.List;

/**
 *
 * @author BELSOFT
 */
public interface BSIDAO<E> {

    public abstract void close();

    public abstract E buscar(Object paramObject);

    public abstract List<E> listar();

    public abstract List<E> listar(String paramString);

    public abstract boolean insertar(E paramObject);

    public abstract E consultar(String paramString);

    public abstract boolean modificar(E paramObject);

    public abstract boolean actualizar(E paramObject);

    public abstract boolean eliminar(E paramObject);

}
