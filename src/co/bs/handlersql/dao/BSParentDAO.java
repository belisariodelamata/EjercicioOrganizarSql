/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.bs.handlersql.dao;

import co.bs.handlersql.util.BSHandlerSql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BELSOFT
 */
public abstract class BSParentDAO {

    BSHandlerSql bsHandlerSql;

    protected static Connection connection;

    static {
        if (BSConnectionProperties.properties.isToConnect()) {
            conectar();
        }
    }

    public BSParentDAO() {
        iniciarHandlerSql();
    }

    /**
     * Se inicializa el Objeto Manejador del Archivo Xml de las SQL teniendo en
     * cuenta que debe tener el mismo nombre del archivo DAO pero en un
     * subpaquete con nombre xml. Es decir, un archivo DAO con direccion
     * co.ejemplo.dao.ArchivoDAO va a ser mapeado con el archivo
     * co.ejemplo.dao.xml.ArchivoDAO.xml
     */
    void iniciarHandlerSql() {
        String rutaArchivo = "/"
                + (this.getClass().getPackage().getName() + ".xml").replaceAll("\\.", "/")
                + "/" + this.getClass().getSimpleName() + ".xml";
        bsHandlerSql = new BSHandlerSql(rutaArchivo);
    }

    protected String getSQL(String nombreConsulta) {
        return bsHandlerSql.getSQL(nombreConsulta);
    }

    static void conectar() {
        try {
            //En los Controladores JDBC V4 no es Necesario
            //Class.forName(driver);
            System.out.println("Conectando con Base de Datos");
            //Paso 2. Abrir la conexión
            connection = DriverManager.getConnection(
                    BSConnectionProperties.properties.getUrl(),
                    BSConnectionProperties.properties.getUser(),
                    BSConnectionProperties.properties.getPassword());
            System.out.println("Conexión Exitosa");
        } catch (Exception ex) {
            logger(BSParentDAO.class, ex);
        }
    }

    protected static void cerrarRsPs(ResultSet rs, PreparedStatement ps) {
        cerrarRs(rs);
        cerrarPs(ps);
    }

    protected static void cerrarPs(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException ex) {
                logger(BSParentDAO.class, ex);
            }
        }
    }

    protected static void cerrarRs(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                logger(BSParentDAO.class, ex);
            }
        }
    }

    static void logger(Class className, Exception ex) {
        Logger.getLogger(className.getName()).log(Level.SEVERE, null, ex);
    }

    protected void logger(Exception ex) {
        logger(this.getClass(), ex);
    }
}
